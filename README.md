## Semestral project for UNIX - vývojové prostredie

### Object
Simple web application build on Flask, which can store location data from monitoring device in database and represent them on the map. 

### TODO
- [x] Handle user login (hashing password, logout, session)
- [x] API for storing location data (url api, API keys handling, keys storage in database and creation)
- [ ] display of map with location data (leaflet.js representation with points on map)
- [ ] Create config file with local configuration (db access, app configuration etc.)
- [ ] Configure permissions to unpublish sensitive data
- [ ] Optimize code
- [ ] Reformat time to local timezone